FROM jenkins/jenkins:lts

USER root

RUN apt-get update && \
    apt-get install -y sudo cmake python3 default-jre && \
    apt-get install -y python3-requests && \
    apt install -y gcc g++ 

CMD ["java", "-jar", "/usr/share/jenkins/jenkins.war"]
